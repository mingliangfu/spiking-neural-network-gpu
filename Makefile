# ===== INFO =====
NAME = cudasnn
CC = g++
NVCC = nvcc
FLAGS = -Wall -Wextra -Werror -std=c++11 -O3
OS = $(shell uname)

# ===== SRCS =====
MAIN_SRCS = main.cpp generate_network.cpp
 TUI_SRCS = main_tui.cpp generate_network.cpp
CUDA_SRCS = simulate_network.cu

MAIN_OBJS = $(MAIN_SRCS:.cpp=.o)
 TUI_OBJS = $(TUI_SRCS:.cpp=.o)
CUDA_OBJS = $(CUDA_SRCS:.cu=.o)

HDRS = SNN.hpp

# ===== INCLUDES / LIBS =====
INCL = -I ./SFML/include
SFML = -framework sfml-graphics -framework sfml-window -framework sfml-system -F SFML/Frameworks
SFML_LINUX = -L./SFML/lib -lsfml-graphics -lsfml-window -lsfml-system
CUDA = -L/usr/local/cuda/lib -L/usr/local/cuda/lib64 -lcuda -L/Developer/NVIDIA/CUDA-7.0/lib -lcudart

SFMLENV = DYLD_FRAMEWORK_PATH="$(shell pwd)/SFML/Frameworks"

# ===== RULES =====
all : $(NAME)
	@if [ ! -d "SFML" ]; then echo "Use 'make sfml' or 'make tui'"; fi
	@if [ -d "SFML" ]; then echo "Use 'make run' to execute the SFML version, or 'make tui' to build the no-SFML version."; fi

$(NAME) : $(MAIN_OBJS) $(CUDA_OBJS) $(HDRS)
ifeq ($(OS), Linux)
	@$(CC) $(FLAGS) -o $(NAME) $(MAIN_OBJS) $(CUDA_OBJS) $(SFML_LINUX) $(CUDA)
else ifeq ($(OS), Darwin)
	@$(CC) $(FLAGS) -o $(NAME) $(MAIN_OBJS) $(CUDA_OBJS) $(SFML) $(CUDA)
	@install_name_tool -change @rpath/libcudart.7.0.dylib /Developer/NVIDIA/CUDA-7.0/lib/libcudart.dylib cudasnn
else
	@echo "Unsupported OS."
endif
	@echo "\033[32mDone\033[0m"

tui : $(TUI_OBJS) $(CUDA_OBJS) $(HDRS)
	@$(CC) $(FLAGS) -o $(NAME) $(TUI_OBJS) $(CUDA_OBJS) $(CUDA)
ifeq ($(OS), Darwin)
	@install_name_tool -change @rpath/libcudart.7.0.dylib /Developer/NVIDIA/CUDA-7.0/lib/libcudart.dylib cudasnn
endif
	@echo "\033[32mDone\033[0m"

sfml: sfmlrem
	mkdir SFML
ifeq ($(OS), Linux)
	wget "http://mirror3.sfml-dev.org/files/SFML-2.3-linux-gcc-64-bit.tar.gz"
	tar -xzf SFML-2.3-linux-gcc-64-bit.tar.gz -C SFML --strip-components=1
	rm -rf "SFML-2.3-linux-gcc-64-bit.tar.gz"
else ifeq ($(OS), Darwin)
	curl -O "http://mirror0.sfml-dev.org/files/SFML-2.2-osx-clang-universal.tar.gz"
	tar -xzf SFML-2.2-osx-clang-universal.tar.gz -C SFML --strip-components=1
	mv SFML/extlibs/freetype.framework SFML/Frameworks/
	rm -rf "SFML-2.2-osx-clang-universal.tar.gz"
else
	@echo "Unsupported OS."
endif

run:
	@DYLD_FRAMEWORK_PATH=`pwd`"/SFML/Frameworks" ./$(NAME)

%.o : %.cpp $(HDRS)
	$(CC) $(FLAGS) -o $@ -c $< $(INCL)

%.o : %.cu
	$(NVCC) $(CUDA_FLAGS) -o $@ -c $<

# ===== RULES - TOOLS =====
clean :
	@rm -f $(MAIN_OBJS) $(TUI_OBJS) $(CUDA_OBJS)

fclean : clean
	@rm -f $(NAME)

re : fclean all

sfmlrem:
	@rm -rf SFML

sfmlenv:
	@echo export $(SFMLENV)

