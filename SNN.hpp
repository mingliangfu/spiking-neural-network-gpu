#ifndef SNN_HPP
# define SNN_HPP
# include <iostream>

# define WIDTH				1280
# define HEIGHT				720
# define XOFFSET			-0
# define YOFFSET			-95

/* Network Settings */
# define STIMULUS			1
# define INHIBITION			-1
# define STIMULUS_RATIO		80
# define SPIKE_BUFFER		4

# define INPUT				0
# define OUTPUT				1
# define HIDDEN				2

typedef struct		s_neuron_info
{
	float			x, y, z;
	char			gid;
	unsigned char	group;			// hidden, input, output
}					t_neuron_info;

typedef struct		s_neuron
{
	short			in_time;
	float			in_value;
	int				next_time;
	float			action_potential;
	float			threshold;		// fire when threshold reached
	float			weight;
	char			type;			// stimulus, inhibition
	char			carry;
}					t_neuron;

typedef struct		s_synapse
{
	int				id_in;
	int				id_out;
	int				axonal_delay;		// in timestep
	bool			status; // NOPENOPE
}					t_synapse;

typedef struct		s_spike
{
	int				syn_id;
	int				id_out;
	int				start_t, end_t;		// in timestep
	float			value;
	bool			active;
}					t_spike;

typedef struct		s_network
{
	t_neuron		*neurons;
	t_neuron_info	*neurons_info;
	t_synapse		*synapses;
	t_spike			*spikes;
	int				neur_count;
	int				syn_count;
	int				timestep;
	int				group_size;
}					t_network;

/* Generation */
t_network			*generate_network(void);

/* CPU Simulation - OLD */
// REMOVE !!
void				crawl_input(t_network *nw);
void				crawl_synapses(t_network *nw);
void				update_spikes(t_network *nw);

/* GPU Simulation */
float				simulate_network(t_network *nw);

#endif
