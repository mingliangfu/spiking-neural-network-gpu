#include <stdlib.h>
#include <time.h>
#include "SNN.hpp"

int			randab(int a, int b)
{
	return (rand() % (b - a) + a);
}

double		frandab(double a, double b)
{
	return ((rand() / (double)RAND_MAX) * (b - a) + a);
}

void		init_neuron(t_neuron_info *nfo, t_neuron *ret, int id,
				unsigned char group, int x, int y, char type)
{
	nfo->x = x;
	nfo->y = y;
	nfo->z = 0;
	nfo->gid = id / 8;
	ret->in_value = frandab(1.0f, 4.0f);
	ret->in_time = randab(30, 40);
	ret->next_time = ret->in_time * frandab(0.2f, 1.0f);
	ret->action_potential = 0.0f;
	ret->threshold = frandab(4.0f, 7.0f);
	ret->weight = frandab(0.5f, 1.8f);
	ret->type = type;
	nfo->group = group;
	ret->carry = 0;
}

void			init_synapse(t_synapse *ret, int in, int out)
{
	ret->id_in = in;
	ret->id_out = out;
	ret->axonal_delay = randab(5, 15);
	ret->status = false;
}

t_network		*generate_network(void)
{
	t_network	*ret;

	if (!(ret = (t_network*)malloc(sizeof(t_network))))
		exit(1);
	ret->timestep = 0;
	ret->group_size = 8;

	/* Alloc */
	ret->neur_count = 20000;
	if (!(ret->neurons = (t_neuron*)malloc(sizeof(t_neuron)
			* ((ret->neur_count + 511) & ~511)))
		|| !(ret->neurons_info = (t_neuron_info*)malloc(sizeof(t_neuron_info)
			* ((ret->neur_count + 511) & ~511))))
		return (NULL);
	ret->syn_count = 400000;
	if (!(ret->synapses = (t_synapse*)malloc(sizeof(t_synapse)
			* ((ret->syn_count + 511) & ~511))))
		return (NULL);
	if (!(ret->spikes = (t_spike*)malloc(sizeof(t_spike)
			* ((ret->syn_count + 511) & ~511) * SPIKE_BUFFER)))
		return (NULL);

	/* Assign */
	{
		/* INPUT */
		init_neuron(&(ret->neurons_info[0]), &(ret->neurons[0]), 0, INPUT, 380, 376, STIMULUS);
		init_neuron(&(ret->neurons_info[1]), &(ret->neurons[1]), 1, INPUT, 424, 512, INHIBITION);
		init_neuron(&(ret->neurons_info[2]), &(ret->neurons[2]), 2, INPUT, 446, 724, STIMULUS);
		init_neuron(&(ret->neurons_info[3]), &(ret->neurons[3]), 3, INPUT, 630, 528, STIMULUS);
		init_neuron(&(ret->neurons_info[4]), &(ret->neurons[4]), 4, INPUT, 574, 166, STIMULUS);
		init_neuron(&(ret->neurons_info[5]), &(ret->neurons[5]), 5, INPUT, 846, 356, STIMULUS);
		init_neuron(&(ret->neurons_info[6]), &(ret->neurons[6]), 6, INPUT, 894, 556, STIMULUS);
		init_neuron(&(ret->neurons_info[7]), &(ret->neurons[7]), 7, INPUT, 1065, 720, STIMULUS);

		/* OUTPUT */
		init_neuron(&(ret->neurons_info[8]), &(ret->neurons[8]), 8, OUTPUT, 220, 580, STIMULUS);
		init_neuron(&(ret->neurons_info[9]), &(ret->neurons[9]), 9, OUTPUT, 140, 444, STIMULUS);
		init_neuron(&(ret->neurons_info[10]), &(ret->neurons[10]), 10, OUTPUT, 626, 430, STIMULUS);
		init_neuron(&(ret->neurons_info[11]), &(ret->neurons[11]), 11, OUTPUT, 360, 134, STIMULUS);
		init_neuron(&(ret->neurons_info[12]), &(ret->neurons[12]), 12, OUTPUT, 650, 138, STIMULUS);
		init_neuron(&(ret->neurons_info[13]), &(ret->neurons[13]), 13, OUTPUT, 878, 165, STIMULUS);
		init_neuron(&(ret->neurons_info[14]), &(ret->neurons[14]), 14, OUTPUT, 958, 285, INHIBITION);
		init_neuron(&(ret->neurons_info[15]), &(ret->neurons[15]), 15, OUTPUT, 995, 461, STIMULUS);

		/* HIDDEN */
		init_neuron(&(ret->neurons_info[16]), &(ret->neurons[16]), 16, INPUT, 322, 556, STIMULUS);
		init_neuron(&(ret->neurons_info[17]), &(ret->neurons[17]), 17, INPUT, 460, 622, STIMULUS);
		init_neuron(&(ret->neurons_info[18]), &(ret->neurons[18]), 18, INPUT, 578, 660, INHIBITION);
		init_neuron(&(ret->neurons_info[19]), &(ret->neurons[19]), 19, INPUT, 560, 478, STIMULUS);
		init_neuron(&(ret->neurons_info[20]), &(ret->neurons[20]), 20, INPUT, 276, 342, STIMULUS);
		init_neuron(&(ret->neurons_info[21]), &(ret->neurons[21]), 21, INPUT, 490, 360, INHIBITION);
		init_neuron(&(ret->neurons_info[22]), &(ret->neurons[22]), 22, INPUT, 630, 342, STIMULUS);
		init_neuron(&(ret->neurons_info[23]), &(ret->neurons[23]), 23, INPUT, 792, 276, STIMULUS);

		/* HIDDEN */
		init_neuron(&(ret->neurons_info[24]), &(ret->neurons[24]), 24, INPUT, 740, 350, STIMULUS);
		init_neuron(&(ret->neurons_info[25]), &(ret->neurons[25]), 25, INPUT, 718, 442, STIMULUS);
		init_neuron(&(ret->neurons_info[26]), &(ret->neurons[26]), 26, INPUT, 808, 420, INHIBITION);
		init_neuron(&(ret->neurons_info[27]), &(ret->neurons[27]), 27, INPUT, 942, 374, STIMULUS);
		init_neuron(&(ret->neurons_info[28]), &(ret->neurons[28]), 28, INPUT, 812, 520, STIMULUS);
		init_neuron(&(ret->neurons_info[29]), &(ret->neurons[29]), 29, INPUT, 964, 516, STIMULUS);
		init_neuron(&(ret->neurons_info[30]), &(ret->neurons[30]), 30, INPUT, 866, 660, STIMULUS);
		init_neuron(&(ret->neurons_info[31]), &(ret->neurons[31]), 31, INPUT, 996, 746, STIMULUS);

		/* HIDDEN */
		init_neuron(&(ret->neurons_info[32]), &(ret->neurons[32]), 32, INPUT, 372, 686, STIMULUS);
		init_neuron(&(ret->neurons_info[33]), &(ret->neurons[33]), 33, INPUT, 300, 472, STIMULUS);
		init_neuron(&(ret->neurons_info[34]), &(ret->neurons[34]), 34, INPUT, 412, 288, INHIBITION);
		init_neuron(&(ret->neurons_info[35]), &(ret->neurons[35]), 35, INPUT, 610, 264, STIMULUS);
		init_neuron(&(ret->neurons_info[36]), &(ret->neurons[36]), 36, INPUT, 1074, 650, STIMULUS);
		init_neuron(&(ret->neurons_info[37]), &(ret->neurons[37]), 37, INPUT, 760, 664, INHIBITION);
		init_neuron(&(ret->neurons_info[38]), &(ret->neurons[38]), 38, INPUT, 902, 776, STIMULUS);
		init_neuron(&(ret->neurons_info[39]), &(ret->neurons[39]), 39, INPUT, 700, 780, STIMULUS);
	}
	{
		int si = 0;
		init_synapse(&(ret->synapses[si++]), 16, 8);
		init_synapse(&(ret->synapses[si++]), 16, 33);
		init_synapse(&(ret->synapses[si++]), 33, 9);
		init_synapse(&(ret->synapses[si++]), 33, 20);
		init_synapse(&(ret->synapses[si++]), 0, 33);
		init_synapse(&(ret->synapses[si++]), 0, 20);
		init_synapse(&(ret->synapses[si++]), 0, 34);
		init_synapse(&(ret->synapses[si++]), 0, 21);

		init_synapse(&(ret->synapses[si++]), 0, 19);
		init_synapse(&(ret->synapses[si++]), 1, 19);
		init_synapse(&(ret->synapses[si++]), 34, 11);
		init_synapse(&(ret->synapses[si++]), 4, 12);
		init_synapse(&(ret->synapses[si++]), 4, 34);
		init_synapse(&(ret->synapses[si++]), 4, 35);
		init_synapse(&(ret->synapses[si++]), 35, 12);
		init_synapse(&(ret->synapses[si++]), 35, 23);

		init_synapse(&(ret->synapses[si++]), 23, 12);
		init_synapse(&(ret->synapses[si++]), 23, 13);
		init_synapse(&(ret->synapses[si++]), 23, 14);
		init_synapse(&(ret->synapses[si++]), 5, 23);
		init_synapse(&(ret->synapses[si++]), 5, 24);
		init_synapse(&(ret->synapses[si++]), 5, 26);
		init_synapse(&(ret->synapses[si++]), 5, 27);
		init_synapse(&(ret->synapses[si++]), 5, 14);

		init_synapse(&(ret->synapses[si++]), 21, 34);
		init_synapse(&(ret->synapses[si++]), 21, 22);
		init_synapse(&(ret->synapses[si++]), 22, 35);
		init_synapse(&(ret->synapses[si++]), 24, 35);
		init_synapse(&(ret->synapses[si++]), 2, 32);
		init_synapse(&(ret->synapses[si++]), 2, 17);
		init_synapse(&(ret->synapses[si++]), 1, 17);
		init_synapse(&(ret->synapses[si++]), 17, 16);

		init_synapse(&(ret->synapses[si++]), 3, 19);
		init_synapse(&(ret->synapses[si++]), 3, 25);
		init_synapse(&(ret->synapses[si++]), 7, 36);
		init_synapse(&(ret->synapses[si++]), 7, 31);
		init_synapse(&(ret->synapses[si++]), 7, 30);
		init_synapse(&(ret->synapses[si++]), 31, 38);
		init_synapse(&(ret->synapses[si++]), 38, 39);
		init_synapse(&(ret->synapses[si++]), 39, 18);

		init_synapse(&(ret->synapses[si++]), 18, 17);
		init_synapse(&(ret->synapses[si++]), 18, 19);
		init_synapse(&(ret->synapses[si++]), 30, 28);
		init_synapse(&(ret->synapses[si++]), 30, 37);
		init_synapse(&(ret->synapses[si++]), 37, 28);
		init_synapse(&(ret->synapses[si++]), 28, 29);
		init_synapse(&(ret->synapses[si++]), 6, 29);
		init_synapse(&(ret->synapses[si++]), 29, 15);

		init_synapse(&(ret->synapses[si++]), 36, 29);
		init_synapse(&(ret->synapses[si++]), 27, 14);
		init_synapse(&(ret->synapses[si++]), 25, 10);
		init_synapse(&(ret->synapses[si++]), 19, 10);
	/*  init_synapse(&(ret->synapses[si++]), , );
		init_synapse(&(ret->synapses[si++]), , );
		init_synapse(&(ret->synapses[si++]), , );
		init_synapse(&(ret->synapses[si++]), , ); */
	}

	for (int i = 0; i < ret->syn_count * SPIKE_BUFFER; ++i)
		ret->spikes[i].active = false;

	return (ret);
}
