#include <SFML/Graphics.hpp>
#include <stdio.h>
#include "SNN.hpp"

#define NEURON_RADIUS 10
#define SYNAPSE_RADIUS 5

sf::CircleShape		neuron_circle, api_circle, ape_circle;
sf::ContextSettings	settings;
sf::RenderWindow	window;
sf::Text			t_id, t_ap;
sf::Font			font;

t_network			*nw;

void			draw_neuron(int x, int y, int id, float ap, int i)
{
	x += XOFFSET; y += YOFFSET;

	neuron_circle.setPosition(x - NEURON_RADIUS, y - NEURON_RADIUS);
	if (id == INPUT)
		neuron_circle.setFillColor(sf::Color(170, 170, 56));
	else if (id == OUTPUT)
		neuron_circle.setFillColor(sf::Color(34, 102, 102));
	else
		neuron_circle.setFillColor(sf::Color(255, 210, 240));
	window.draw(neuron_circle);

	(void)i;
	t_id.setString(std::to_string(id).c_str());
	t_id.setPosition(x - 2, y - 5);
	t_id.setColor(sf::Color::Black);
	window.draw(t_id);

	char *ret; if ((asprintf(&ret, "%.1f", ap)) == -1) exit(2);
	t_ap.setString(ret);
	t_ap.setPosition(x + 12, y - 8);
	window.draw(t_ap);
	free(ret);
}

void			draw_synapse(int x1, int y1, int x2, int y2)
{
	x1 += XOFFSET; y1 += YOFFSET;
	x2 += XOFFSET; y2 += YOFFSET;

	sf::Vertex line[] =
	{
		sf::Vertex(sf::Vector2f(x1, y1), sf::Color(200, 200, 200, 80)),
		sf::Vertex(sf::Vector2f(x2, y2), sf::Color(40, 40, 40, 80))
	};
	window.draw(line, 2, sf::Lines);
}

void			draw_spike(int x1, int y1, int x2, int y2,
		int type, float progress)
{
	x1 += XOFFSET; y1 += YOFFSET;
	x2 += XOFFSET; y2 += YOFFSET;

	if (type == STIMULUS) {
		int nx = x1 + ((x2-x1) * progress) - SYNAPSE_RADIUS;
		int ny = y1 + ((y2-y1) * progress) - SYNAPSE_RADIUS;
		ape_circle.setPosition(nx, ny);
		window.draw(ape_circle);
	} else if (type == INHIBITION) {
		int nx = x1 + ((x2-x1) * progress) - SYNAPSE_RADIUS;
		int ny = y1 + ((y2-y1) * progress) - SYNAPSE_RADIUS;
		api_circle.setPosition(nx, ny);
		window.draw(api_circle);
	}
}

int				get_active_spikes(void)
{
	int		act;

	act = 0;
	for (int i = 0; i < nw->syn_count * SPIKE_BUFFER; ++i)
		if (nw->spikes[i].active)
			++act;
	return (act);
}

void			display_network(float t, bool paused)
{
	/* DISPLAY SYNAPSES */
	for (int i = 0; i < (nw->syn_count > 8000 ? 8000 : nw->syn_count); ++i)
		draw_synapse(nw->neurons_info[nw->synapses[i].id_in].x,
				nw->neurons_info[nw->synapses[i].id_in].y,
				nw->neurons_info[nw->synapses[i].id_out].x,
				nw->neurons_info[nw->synapses[i].id_out].y);

	/* DISPLAY SPIKES */
	for (int i = 0; i < (nw->syn_count > 6000 ? 6000 : nw->syn_count) * SPIKE_BUFFER; ++i)
		if (nw->spikes[i].active)
		{
			draw_spike(
					nw->neurons_info[nw->synapses[nw->spikes[i].syn_id].id_in].x,
					nw->neurons_info[nw->synapses[nw->spikes[i].syn_id].id_in].y,
					nw->neurons_info[nw->synapses[nw->spikes[i].syn_id].id_out].x,
					nw->neurons_info[nw->synapses[nw->spikes[i].syn_id].id_out].y,
					nw->spikes[i].value >= 0 ? STIMULUS : INHIBITION,
					1 - ((nw->spikes[i].end_t - nw->timestep)
						/ (float)(nw->spikes[i].end_t - nw->spikes[i].start_t)));
		}

	/* DISPLAY NEURONS */
	for (int i = 0; i < (nw->neur_count > 2000 ? 2000 : nw->neur_count); ++i)
		draw_neuron(nw->neurons_info[i].x, nw->neurons_info[i].y,
				nw->neurons_info[i].gid, nw->neurons[i].action_potential, i);

	/* DISPLAY TIMESTEP */
	char *ret; if ((asprintf(&ret, "Timestep: %d\nTime: %.4fms\nActivity: %.1f%%%s",
			nw->timestep, t, get_active_spikes() / (float)nw->neur_count * 100.0f,
			paused ? "\nPAUSED" : "")) == -1) exit(2);
	t_ap.setString(ret); t_ap.setPosition(4, 2);
	t_ap.setColor(sf::Color::White);
	window.draw(t_ap); free(ret);
}

int				main()
{
	sf::Event	ev;
	float		elapsed = 0.0f;
	bool		paused = false;

	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;
	window.create(sf::VideoMode(WIDTH, HEIGHT), "GPU SNN - Visualizer", sf::Style::Default, settings);

	srand(time(NULL));
	neuron_circle.setRadius(NEURON_RADIUS);
	api_circle.setRadius(SYNAPSE_RADIUS);
	ape_circle.setRadius(SYNAPSE_RADIUS);
	neuron_circle.setFillColor(sf::Color(68, 86, 82));
	api_circle.setFillColor(sf::Color(200, 90, 54));
	ape_circle.setFillColor(sf::Color(118, 180, 48));

	if (!font.loadFromFile("arial.ttf"))
		std::cerr << "Font error." << std::endl;
	t_id.setFont(font); t_ap.setFont(font);
	t_id.setCharacterSize(9); t_ap.setCharacterSize(12);

	nw = generate_network();
	printf("Network generated.\n");
	window.setFramerateLimit(40);
	while (window.isOpen())
	{
		while (window.pollEvent(ev) || 1)
		{
			if (ev.type == sf::Event::Closed)
				window.close();
			/* KEYBINDS */
			else if (ev.type == sf::Event::KeyPressed)
			{
				if (ev.key.code == sf::Keyboard::Escape)
					return (0);
				else if (ev.key.code == sf::Keyboard::Space)
					paused = !paused;
			}

			/* DISPLAY */
			window.clear(sf::Color(34, 34, 34));
			display_network(elapsed, paused);
			window.display();

			/* SIMULATION */
			if (!paused)
			{
				++nw->timestep;
				elapsed = simulate_network(nw);
			}

		}
	}
	return (0);
}
