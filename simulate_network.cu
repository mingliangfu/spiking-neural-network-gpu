#include <cuda.h>
#include <stdio.h>
#include "SNN.hpp"

#define cudaErrorAbort(msg) \
	{ \
		cudaError_t __err = cudaGetLastError(); \
		if (__err != cudaSuccess) { \
			fprintf(stderr, "[CUDA Error] %s : %s (%s:%d)\n", \
					msg, cudaGetErrorString(__err), \
					__FILE__, __LINE__); \
			exit(1); \
		} \
	}


/* First Kernel
 * ============
 * Neuronal parallelism on the INPUT group,
 * check if an INPUT neuron should fire or not.
 */
__global__ void		check_input(t_neuron *n, int timestep)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;

	if (timestep >= n[idx].next_time)
	{
		n[idx].action_potential += n[idx].in_value;
		n[idx].next_time += n[idx].in_time;
	}
}

/* Second Kernel
 * =============
 * Synaptic parallelism on every synapses,
 * check pre-synaptic neurons and create a spike if AP >= threshold
 */
__global__ void		check_synapses(t_neuron *n, t_synapse *s, t_spike *sp, int timestep)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;

	if (n[s[idx].id_in].action_potential < n[s[idx].id_in].threshold)
		return ;
	n[s[idx].id_in].carry = 1;

	/* nw->spikes is a multi-circular coalesced buffer */
	for (int i = idx * SPIKE_BUFFER; i < idx * SPIKE_BUFFER + SPIKE_BUFFER; ++i)
	{
		if (sp[i].active)
			continue;

		sp[i].syn_id = idx;
		sp[i].id_out = s[idx].id_out;
		sp[i].start_t = timestep;
		sp[i].end_t = timestep + s[idx].axonal_delay;
		sp[i].value = n[s[idx].id_in].action_potential * 0.2f * n[s[idx].id_in].type;
		sp[i].active = true;
		break ;
	}
}
__global__ void		carry_reset(t_neuron *n)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;

	/* Continuous decay */
	// n[idx].action_potential -= 0.005f;
	// if (n[idx].action_potential < 0.0f)
	//	n[idx].action_potential = 0.0f;

	/* Might be faster depending of the architecture */
	// n[idx].action_potential -= (n[idx].carry * n[idx].action_potential);
	// n[idx].carry = 0;

	if (n[idx].carry)
	{
		n[idx].action_potential = 0.0f;
		n[idx].carry = 0;
	}
}


/* Third Kernel
 * ============
 * Process the multi-circular buffer by batch of SPIKE_BUFFER, and update
 * the AP of the post-synaptic neuron (+ kill the spike) if needed
 */
__global__ void		check_spikes(t_neuron *n, t_spike *sp, int timestep)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;

	for (int i = idx * SPIKE_BUFFER; i < idx * SPIKE_BUFFER + SPIKE_BUFFER; ++i)
	{
		if (timestep >= sp[i].end_t)
		{
			sp[i].active = false;
			sp[i].end_t = INT_MAX;		// to avoid the 'continue' branching
			n[sp[i].id_out].action_potential += (sp[i].value * n[sp[i].id_out].weight);

			if (n[sp[i].id_out].action_potential < 0.0f)
				n[sp[i].id_out].action_potential = 0.0f;
		}
	}
}

float		simulate_network(t_network *nw)
{
	static t_neuron		*neur_cptr = NULL;
	static t_synapse	*syn_cptr = NULL;
	static t_spike		*spike_cptr = NULL;
	static size_t		cp_size = sizeof(t_neuron) * ((nw->neur_count + 511) & ~511);
	static size_t		cp_syn_size = sizeof(t_synapse) * ((nw->syn_count + 511) & ~511);
	static size_t		cp_spike_size = sizeof(t_spike) * ((nw->syn_count + 511) & ~511) * SPIKE_BUFFER;
	/* add some more space to avoid useless conditions in kernel */

	if (!neur_cptr)
	{
		/* Malloc/cpy for neurons/synapses/spikes */
		cudaMalloc((void**) &neur_cptr, cp_size);
		cudaMemcpy(neur_cptr, nw->neurons, cp_size, cudaMemcpyHostToDevice);
		cudaMalloc((void**) &syn_cptr, cp_syn_size);
		cudaMemcpy(syn_cptr, nw->synapses, cp_syn_size, cudaMemcpyHostToDevice);
		cudaMalloc((void**) &spike_cptr, cp_spike_size);
		cudaMemcpy(spike_cptr, nw->spikes, cp_spike_size, cudaMemcpyHostToDevice);
		cudaErrorAbort("CUDA Malloc/Memcpy Error.");
	}

	/* Block size/N for kernel 1, 2 and 3 */
	/* size condition to avoid future (small) divergent warps on small networks */
	int syn_block_size = 512 > nw->syn_count ? nw->syn_count : 512;
	int syn_block_count = nw->syn_count / syn_block_size
		+ (!(nw->syn_count % syn_block_size) ? 0 : 1);
	int carry_block_size = 512 > nw->neur_count ? nw->neur_count : 512;
	int carry_block_count = nw->neur_count / carry_block_size
		+ (!(nw->neur_count % carry_block_size) ? 0 : 1);

	cudaEvent_t start, stop;
	float elapsed_time;

	cudaEventCreate(&start);
	cudaEventRecord(start, 0);

	/* Kernel call */
	check_input <<< 1, nw->group_size >>> (neur_cptr, nw->timestep);
	cudaDeviceSynchronize();
	cudaErrorAbort("CUDA Kernel Error 1.");

	check_synapses <<< syn_block_count, syn_block_size >>> (neur_cptr,
			syn_cptr, spike_cptr, nw->timestep);
	cudaDeviceSynchronize();
	cudaErrorAbort("CUDA Kernel Error 2.");

	carry_reset <<< carry_block_count, carry_block_size >>> (neur_cptr);
	cudaDeviceSynchronize();
	cudaErrorAbort("CUDA Kernel Error 3.");

	check_spikes <<< syn_block_count, syn_block_size >>> (neur_cptr,
			spike_cptr, nw->timestep);
	cudaDeviceSynchronize();
	cudaErrorAbort("CUDA Kernel Error 4.");

	cudaEventCreate(&stop); cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop); cudaEventElapsedTime(&elapsed_time, start, stop);
	printf("[%04d] Elapsed time: %f ms\n", nw->timestep, elapsed_time);

	/* Cpy back */
	cudaMemcpy(nw->neurons, neur_cptr, cp_size, cudaMemcpyDeviceToHost);
	cudaMemcpy(nw->synapses, syn_cptr, cp_syn_size, cudaMemcpyDeviceToHost);
	cudaMemcpy(nw->spikes, spike_cptr, cp_spike_size, cudaMemcpyDeviceToHost);
	cudaErrorAbort("CUDA Memcpy eError.");

	return (elapsed_time);
}
